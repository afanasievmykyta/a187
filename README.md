# Interpolation Search App

### Check if you have npm and node.js
```
node -v
npm -v
```
### Download node.js and npm 
```
sudo apt install nodejs
```
### Usually npm installed with node.js, but you can install separately 

```
sudo apt install npm
```

### Project setup
```
npm i
```

### Compile and get access to project
```
npm run serve
```

### You can access app [here](http://localhost:8080/)


# Interpolation Search

Interpolation Search is improved version over Binary search. Actually Interpolation search is closer to human's search.

Binary search always go to center of array, slicing it, take needed array and slice it again in the center until needed value will be found.

On other hand, let's check how humans usually search. First of all we analyze to which end needed value is closer. 
Then we choose random value, which is closer to chosen end. It's actually slices our array on 2 parts. 
If our value is bigger, than needed one, we continue to search in left side of sliced array, if smaller, than opposite. 
We repeat until we found needed value.

That how actually Interpolation Search works.
Usually Interpolation's search time complexity is log(log(n)). In the worst case (when values increase exponentially) it can grow up to O(n). 

To compare Binary's search time complexity is O(log(n)) in average search. Also in the worst case, Binary search will still run with logarithmic complexity.   

To find the position to be searched, it uses following formula. 

```
// The idea of formula is to return higher value of pos
// when element to be searched is closer to arr[hi]. And
// smaller value when closer to arr[lo]
pos = lo + [ (x-arr[lo])*(hi-lo) / (arr[hi]-arr[Lo]) ]

arr[] ==> Array where elements need to be searched
x     ==> Element to be searched
lo    ==> Starting index in arr[]
hi    ==> Ending index in arr[]
```

### In this app
In this app will be presented demonstration of Interpolation search.
What can user do: 
- Manage a length of created array
- Check created array
- Choose value that he wants to find
- Follow where app was on each step of search
- New sliced arrays will be also displayed, so user can follow more precisely how search works
- Can see result of search, with steps amount needed to reach searched value

### References
- [GeeksForGeeks](https://www.geeksforgeeks.org/interpolation-search/)
- [Wikipedia](https://en.wikipedia.org/wiki/Interpolation_search)
- [tutorialspoint](https://www.tutorialspoint.com/data_structures_algorithms/interpolation_search_algorithm.htm)